'use strict';

const express = require('express'),
    router = express.Router();

const storageLayer = require('./storageLayer');

/* GET getLogsByContainerId listing. */
router.get('/', (req, res) => {
    try {
        const containerId = req.query.containerId;
        if (!containerId)
            return res.status(500).send('containerId required');

        if (!storageLayer.existsByContainerId(containerId))
            return res.status(500).send('containerId not exists');

        res.status(200).send(storageLayer.getLogsByContainerId(containerId));
    } catch (e) {
        console.log(e.message)
        res.status(500).send(e.message);
    }
});

module.exports = router;
