'use strict';

const storageLayer = require('./storageLayer');
const Docker = require('dockerode');


module.exports.start = () => {
    console.log('Start Docker Attach');

    let docker = new Docker(); // default new Docker({socketPath: '/var/run/docker.sock'});
    const remoteDockerDaemonUrl = process.env.REMOTE_DOCKER_DAEMON_URL;
    if (remoteDockerDaemonUrl) {
        console.log('Using REMOTE_DOCKER_DAEMON_URL: '+remoteDockerDaemonUrl);
        docker = new Docker({dockerDaemonUrl: remoteDockerDaemonUrl});
    }

    const attach_opts = {
        logs: true,
        stream: true,
        stdout: true,
        stderr: true
    };

    process.env.ATTACH_TO_CONTAINERS.split(',').forEach(containerId => {
        const container = docker.getContainer(containerId);
        if (!container) {
            console.log(`${containerId} not exists`);
            return
        }

        console.log(`Attaching to ${containerId}`);

        container.attach(attach_opts, (err, stream) => {
            new Promise((res, rej) => {
                if (err) {
                    return rej(err);
                }
                return res(stream);
            }).then((stream) => {
                storageLayer.save(stream, containerId);
            }).catch((err) => {
                console.log(err);
            })
        })
    })
}
