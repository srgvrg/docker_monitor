const { exec } = require('child_process');
const fs = require('fs');

let wstream = fs.createWriteStream('myOutput.txt');

let container = 'topdemo'

exec(`docker attach ${container}`, (error, stdout, stderr) => {
    console.log(`DockerMonitor attached to ${container} container`)
    let out = error ? error : stdout;
    console.log(`out: ${out}`);
    wstream.write(`${out}`);
    wstream.end(function () { console.log('done'); });
    return;
});



// stream.on('data', function (data) {
//     const lines = data.toString('utf8');
//     const logsArr = lines.split('\n').map(line => {
//         let logLine = {
//             timestamp: new Date(),
//             containerId: containerId,
//             logLine: line
//         }
//         console.log(JSON.stringify(logLine))
//         return logLine;
//     });
//     wstream.writable(logsArr, "ASCII")
// })