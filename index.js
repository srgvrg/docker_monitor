'use strict';

const express = require('express');
const app = express();

app.use('/getLogsByContainerId', require('./logsRouter'));

app.listen(8080, () => {
    console.log("Server running on port 8080");
});

require('./dockerAttach').start();
