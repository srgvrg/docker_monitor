'use strict';

const fs = require('fs');

module.exports.save = (stream, containerId) => {
    let wstream = fs.createWriteStream(`./storage/${containerId}-logs.json`);

    let readline = require('readline').createInterface({
        input: stream,
        terminal: false
    });

    readline.on('line', (line) => {
        let logLine = {
            timestamp: new Date().getTime(),
            logLine: line.replace(/\0/g, '')};

        let json = JSON.stringify(logLine)+'\n';
        // console.log(`Saving ${json}`);
        wstream.write(json);
    });

};

module.exports.existsByContainerId = (containerId) => {
    return fs.existsSync(`./storage/${containerId}-logs.json`)
};

module.exports.getLogsByContainerId = (containerId) => {
    return fs.readFileSync(`./storage/${containerId}-logs.json`).toString()
};
