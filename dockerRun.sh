#!/usr/bin/env bash

docker rm -f docker_monitor | true && \
docker rmi -f docker_monitor | true && \
docker build -t docker_monitor . && \
docker run -p 49160:8080 -d --name=docker_monitor -e ATTACH_TO_CONTAINERS=test1,test2 -v /var/run/docker.sock:/var/run/docker.sock docker_monitor && \
docker logs -f docker_monitor

curl 0:49160/getLogsByContainerId?containerId=test

docker rm -f docker_monitor | true && \
docker rmi -f docker_monitor | true && \
docker build -t docker_monitor . && \
docker run -p 49160:8080 -d --name=docker_monitor -e ATTACH_TO_CONTAINERS=test1 -e REMOTE_DOCKER_DAEMON_URL=<URL> docker_monitor && \
docker logs -f docker_monitor


