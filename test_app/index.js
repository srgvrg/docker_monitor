'use strict';

const express = require('express');
const app = express();

app.get('/log', (req, res) => {
    console.log(req.query.log)
    res.send("printed: "+req.query.log)
});

app.listen(8080, () => {
    console.log("Server running on port 8080");
});