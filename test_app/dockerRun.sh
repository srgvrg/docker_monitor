#!/usr/bin/env bash

docker rm -f test1 | true && \
docker rmi -f test1  | true && \
docker build -t test1 . && \
docker run -p 49161:8080 -d --name=test1 test1 && \
docker logs -f test1


docker rm -f test2 | true && \
docker rmi -f test2  | true && \
docker build -t test2 . && \
docker run -p 49162:8080 -d --name=test2 test2 && \
docker logs -f test2

curl  0.0.0.0:49162/log?log=kuku

curl  0.0.0.0:49161/log?log=blabla
