# Docker Monitor

Existing Capabilities:
* App runs as Docker container
* By default this app uses local host docker daemon via docker volume `-v /var/run/docker.sock:/var/run/docker.sock `
* Optional may use remote host docker daemon `-e REMOTE_DOCKER_DAEMON_URL=<URL>`
* Attach to multiple containers `-e ATTACH_TO_CONTAINERS=test1,test2 `
* After attach gets stream of all logs from predefined containers 
* StoreLayer gets stream of each container attach and stores logs in json files in `./storage` path
* API for fetch logs of required container `curl 0:49160/getLogsByContainerId?containerId=test`


How to use:
* Build docker image  `docker build -t docker_monitor .`

* Run container with various options:
  * `docker run  -p 49160:8080  -d  --name=docker_monitor  -e ATTACH_TO_CONTAINERS=test1,test2  -v /var/run/docker.sock:/var/run/docker.sock  docker_monitor`
  
  set containers to attach (with comma)
  * `-e ATTACH_TO_CONTAINERS=test1,test2 `
  
  use local host docker daemon socket OR remote host docker daemon
  * `-v /var/run/docker.sock:/var/run/docker.sock ` (default)
  * `-e REMOTE_DOCKER_DAEMON_URL=<URL>` (optional)
  

Test/Demo:
* Create test containers
  * `cd ./test_app`
  * `docker build -t test_app .`
  * `docker run -p 49161:8080 -d --name=test_app1 test_app`
  * `docker run -p 49162:8080 -d --name=test_app2 test_app`

* Create app image
  * `cd ../`  
  * `docker build -t docker_monitor .`
  * `docker run  -p 49160:8080  -d  --name=docker_monitor  -e ATTACH_TO_CONTAINERS=test_app1,test_app2  -v /var/run/docker.sock:/var/run/docker.sock  docker_monitor`

* Add logs to test containers
  * `curl  0:49161/log?log=koko`
  * `curl  0:49162/log?log=kuku`

* Get logs by containerId
  * `curl 0:49160/getLogsByContainerId?containerId=test_app1`
  * `curl 0:49160/getLogsByContainerId?containerId=test_app2`
